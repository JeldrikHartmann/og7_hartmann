package fibonacci;

public class Fibonacci {

	public Fibonacci() {
		// TODO Auto-generated constructor stub
	}

	public long fibo(int a) {
		if (a == 1 || a == 2)
			return 1;
		else
			return fibo(a - 1) + fibo(a - 2);
	}

}
