package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet extends Position{

	// Attribute
	private int anzahlHafen;
	private String name;
	// Methoden
	public Planet() {
		super();
	}
	

	public Planet(double posX, double posY, int anzahlHafen, String name) {
		super();
		this.anzahlHafen = anzahlHafen;
		this.name = name;
	}




	public int getAnzahlHafen() {
		return anzahlHafen;
	}


	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
