package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends Position{

	// Attribute
	private int maxKapazitaet;
	private String typ;
	private String antrieb;
	private int winkel;
	// Methoden
	public Raumschiff() {
		super();
	}


	public Raumschiff(double posX, double posY, int maxKapazitaet, String typ, String antrieb, int winkel) {
		super();
		this.maxKapazitaet = maxKapazitaet;
		this.typ = typ;
		this.antrieb = antrieb;
		this.winkel = winkel;
	}




	public int getMaxLadekapazitaet() {
		return maxKapazitaet;
	}


	public void setMaxLadekapazitaet(int maxKapazitaet) {
		this.maxKapazitaet = maxKapazitaet;
	}


	public String getTyp() {
		return typ;
	}


	public void setTyp(String typ) {
		this.typ = typ;
	}


	public String getAntrieb() {
		return antrieb;
	}


	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}


	public int getWinkel() {
		return winkel;
	}


	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}


	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
