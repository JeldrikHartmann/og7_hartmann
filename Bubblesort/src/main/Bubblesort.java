package main;

public class Bubblesort {

	public void bubblesort(int[] a, int l, int r) {
		for (int k = l; k < r; k++) {
			if (a[k] > a[k + 1]) {
				for (int i = r; i > l; i--) {

					for (int j = l; j < i; j++) {
						if (a[j] > a[j + 1] && j < a.length - 1) {
							int n = a[j];
							a[j] = a[j + 1];
							a[j + 1] = n;
						}
					}
				}
			}
		}
	}

	public String gibArrayAus(int[] a) {

		for (int i = 0; i < a.length - 1; i++)
			System.out.print(a[i] + ", ");
		return a[a.length - 1] + "\n";
	}
}
