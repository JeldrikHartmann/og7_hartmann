package main;

public class Zeitschrift extends Medien {
	private String issn;

	public Zeitschrift(String autor, String titel, String ean, String issn) {
		super(autor, titel, ean);
		this.issn = issn;
	}

	public String getIssn() {
		return issn;
	}

	public void setIsbn(String issn) {
		this.issn = issn;
	}

	@Override
	public String toString() {
		return "Autor: " + getAutor() + ", Titel: " + getTitel() + ", EAN: " + getEan() + ", ISSN: " + getIssn();
	}

	@Override
	public boolean equals(Object o) {
		return this == o;
	}

	public int compareTo(Medien andere) {
		if (andere instanceof Zeitschrift)
			return this.getIssn().compareTo(((Zeitschrift) andere).getIssn());
		else
			return super.compareTo(andere);
	}

}
