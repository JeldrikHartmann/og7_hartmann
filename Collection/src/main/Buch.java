package main;

public class Buch extends Medien {
	private String isbn;

	public Buch(String autor, String titel, String ean, String isbn) {
		super(autor, titel, ean);
		this.isbn = isbn;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Autor: " + getAutor() + ", Titel: " + getTitel() + ", EAN: " + getEan() + ", ISBN: " + getIsbn();
	}

	@Override
	public boolean equals(Object o) {
		return this == o;
	}

	public int compareTo(Medien andere) {
		if (andere instanceof Buch)
			return this.getIsbn().compareTo(((Buch) andere).getIsbn());
		else
			return super.compareTo(andere);
	}

}
