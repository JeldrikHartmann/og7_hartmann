package main;

public class Medien implements Comparable<Medien> {
	protected String autor;
	protected String titel;
	protected String ean;

	public Medien(String autor, String titel, String ean) {
		this.autor = autor;
		this.titel = titel;
		this.ean = ean;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String isbn) {
		this.ean = isbn;
	}

	@Override
	public String toString() {
		return "Autor: " + getAutor() + ", Titel: " + getTitel() + ", EAN: " + getEan();
	}

	@Override
	public boolean equals(Object o) {	
		return this == o;
	}

	@Override
	public int compareTo(Medien aMedie) {
		return this.getEan().compareTo(aMedie.getEan());
	}

}
