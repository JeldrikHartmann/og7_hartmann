package main;

import java.util.*;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Medien-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr�sste Medie");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Medien> medienliste = new LinkedList<Medien>();
		Scanner myScanner = new Scanner(System.in);

		char wahl;

		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Wer ist der Autor der Medie?");
				String autor = myScanner.next();
				System.out.println("Wie soll die Medie heissen?");
				String titel = myScanner.next();
				System.out.println("Wie lautet die EAN der Medie?");
				String ean = myScanner.next();
				System.out.println("Welche Kategorie soll die neue Medie haben? (Buch|zeitschrift)");
				String medie = myScanner.next();
				if(medie.equalsIgnoreCase("buch")) {
					System.out.println("Welche ISBN soll das Buch haben?");
					String isbn = myScanner.next();
					medienliste.add(new Buch(autor, titel, ean, isbn));
				}
				else if(medie.equalsIgnoreCase("zeitschrift")) {
					System.out.println("Welche ISSN soll die Zeitschrift haben?");
					String issn = myScanner.next();
					medienliste.add(new Zeitschrift(autor, titel, ean, issn));
				}
				
				break;
			case '2':
				System.out.println("Wie lautet die EAN der gesuchten Medie?");
				String suche = myScanner.next();
				String ergebnis = findeMedie(medienliste, suche);
				if (ergebnis != null) {
					System.out.println(ergebnis);
				} else {
					System.out.println("Die Medie befindet sich nicht in der Liste!");
				}
				break;
			case '3':
				System.out.println("Welcher Index ist der Medie zugewiesen?");
				int l�sche = myScanner.nextInt();
				if (loescheMedie(medienliste, l�sche)) {
					System.out.println("Medie wurde gel�scht!");
				} else {
					System.out.println("Medie wurde nicht gel�scht!");
				}
				break;
			case '4':
				System.out.println("Die Gr��te Medie lautet: " + ermitteleGroessteMedie(medienliste));
				break;
			case '5':
				System.out.println("Welche Medie soll gezeigt werden? (Index)");
				int zeige = myScanner.nextInt();
				System.out.println(medienliste.get(zeige).toString());
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	public static String findeMedie(List<Medien> medienliste, String isbn) {
		for (int i = 0; i < medienliste.size(); i++) {
			if (isbn.equals(medienliste.get(i).getEan())) {
				return medienliste.get(i).toString();
			}
		}
		return null;
	}

	public static boolean loescheMedie(List<Medien> medienliste, int index) {
		if (index < medienliste.size()) {
			medienliste.remove(index);
			return true;
		} else {
			return false;
		}

	}

	public static String ermitteleGroessteMedie(List<Medien> medienliste) {
		Medien gr��teIsbn = medienliste.get(0);
		for (int i = 1; i < medienliste.size(); i++) {
			if (gr��teIsbn.compareTo(medienliste.get(i)) < 0) {
				gr��teIsbn = medienliste.get(i);
			}
		}
		return gr��teIsbn.toString();
	}

}
