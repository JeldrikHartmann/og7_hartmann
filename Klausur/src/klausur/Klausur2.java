package klausur;

import java.util.Random;

public class Klausur2 {

	public final int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		Klausur2 k2 = new Klausur2();
		int n = 1000; // Anzahl der Stellen

		// Erstellt und sortiert Array mit n Stellen
		long[] zahlen = k2.getSortedLongArray(n);

		// ----------Hier in die Main k�nnen Sie Ihre Analyse f�r das Protokoll
		// Lineare Suche: Best Case: O(1), Average Case: O(n), Worst Case: O(n)
		// Schlaubi Schlumpf Suche: Best Case: O(1), Average Case: O(n), Worst Case:
		// O(n)
		// Bin�re Suche: Best Case: O(1), Average Case: O(log(n)), Worst Case: O(log(n))
		// schreiben--------------
		System.out.println("\nLineare Suche:");
		System.out.println("--------------");
		for (long l : zahlen)
			System.out.println(k2.lineareSuche(zahlen, l));
		System.out.println("\nSchlaubi Schlumpf Suche:");
		System.out.println("------------------------");
		for (long l : zahlen)
			System.out.println(k2.schlaubiSchlumpfSuche(zahlen, l));
		System.out.println("\nBin�re Suche:");
		System.out.println("-------------");
		for (long l : zahlen)
			System.out.println(k2.binaereSuche(zahlen, l));

	}

	public String lineareSuche(long[] zahlen, long gesuchteZahl) {
		int vergleiche = 0;
		for (int i = 0; i < zahlen.length; i++) {
			vergleiche++; // f�r for()
			vergleiche++; // f�r if()
			if (zahlen[i] == gesuchteZahl) {

				return i + ", Vergleiche: " + vergleiche;
			}
		}
		vergleiche++; // f�r for() wenn i >= zahlen.length
		return NICHT_GEFUNDEN + ", Vergleiche: " + vergleiche;
	}

	public String schlaubiSchlumpfSuche(long[] zahlen, long gesuchteZahl) {
		int vergleiche = 0;
		int links = 0;
		int rechts = zahlen.length / 4;
		for (int viertel = 1; viertel < 4; viertel++) {
			vergleiche++; // f�r for()
			vergleiche++; // f�r if()
			if (zahlen[rechts] < gesuchteZahl) {
				links = rechts;
				rechts = (viertel + 1) * zahlen.length / 4 - 1;
			}
		}
		vergleiche++; // f�r for() wenn viertel > 4
		while (rechts >= links) {
			vergleiche++; // f�r while()
			vergleiche++; // f�r if(),else()
			if (gesuchteZahl == zahlen[rechts]) {
				return rechts + ", Vergleiche: " + vergleiche;
			} else {
				rechts--;
			}
		}
		vergleiche++; // f�r while() wenn rechts < links
		return NICHT_GEFUNDEN + ", Vergleiche: " + vergleiche;
	}

	public String binaereSuche(long[] zahlen, long gesuchteZahl) {
		int l = 0, r = zahlen.length - 1;
		int m;
		int vergleiche = 0;

		while (l <= r) {
			vergleiche++;
			// Bereich halbieren
			m = l + ((r - l) / 2);
			vergleiche++; // f�r if()
			if (zahlen[m] == gesuchteZahl) { // Element gefunden?
				return m + ", Vergleiche: " + vergleiche;
			}
			vergleiche++; // f�r if(),else()
			if (zahlen[m] > gesuchteZahl)
				r = m - 1; // im linken Abschnitt weitersuchen
			else
				l = m + 1; // im rechten Abschnitt weitersuchen
		}
		vergleiche++; // f�r while() wenn l > r
		return NICHT_GEFUNDEN + ", Vergleiche: " + vergleiche;
	}

	// -----------------------Finger weg von diesem Teil des Codes

	/**
	 * Methode f�llt das Attribut zahlen mit einem Array der L�nge des Paramaters
	 * z.B. getIntArray(1000) gibt ein 1000-stelliges Array zur�ck
	 * 
	 * @param stellen
	 *            - Anzahl der zuf�lligen Stellen
	 * @return das Array
	 */
	public long[] getSortedLongArray(int stellen) {
		Random rand = new Random(666L);
		long[] zahlen = new long[stellen];
		for (int i = 0; i < stellen; i++)
			zahlen[i] = rand.nextInt(20000000);
		this.radixSort(zahlen);
		return zahlen;
	}

	/**
	 * Super effizienter Algorithmus zur Sortierung der Zahlen Laufzeit von
	 * //NoHintAvailable
	 */
	public void radixSort(long[] zahlen) {
		long[] temp = new long[20000000];
		for (long z : zahlen)
			temp[(int) z]++;
		int stelle = 0;
		for (int i = 0; i < temp.length; i++)
			if (temp[i] != 0)
				while (temp[i]-- != 0)
					zahlen[stelle++] = i;
	}

}
