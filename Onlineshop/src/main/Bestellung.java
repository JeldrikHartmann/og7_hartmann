package main;

public class Bestellung {

	private String bestellId;
	private String bestellDat;
	public Bestellung() {
		super();
	}
	public Bestellung(String bestellId, String bestellDat) {
		super();
		this.bestellId = bestellId;
		this.bestellDat = bestellDat;
	}
	public String getBestellId() {
		return bestellId;
	}
	public void setBestellId(String bestellId) {
		this.bestellId = bestellId;
	}
	public String getBestellDat() {
		return bestellDat;
	}
	public void setBestellDat(String bestellDat) {
		this.bestellDat = bestellDat;
	}
	public void artikelHinzufügen() {
		
	}
	public void artikelEntfernen() {
		
	}
	public void bestellungAbschicken() {
		
	}
	@Override
	public String toString() {
		return "Bestellung [bestellId=" + bestellId + ", bestellDat=" + bestellDat + "]";
	}
}
