package main;

public class Kunde extends Benutzer {

	private String regDat;

	public Kunde() {
		super();
	}

	public Kunde(String regDat) {
		super();
		this.regDat = regDat;
	}

	public String getRegDat() {
		return regDat;
	}

	public void setRegDat(String regDat) {
		this.regDat = regDat;
	}
	
	public void löscheDatei() {
		
	}

	@Override
	public String toString() {
		return "Kunde [regDat=" + regDat + "name=" + name + ", email=" + email + ", benutzerId=" + benutzerId + ", passwort=" + passwort + "]";
	}
}
