package main;

public class Administrator extends Benutzer {

	private String personalNr;

	public Administrator() {
		super();
	}

	public Administrator(String personalNr, String name, String email, String benutzerId, String passwort) {
		super(name, email, benutzerId, passwort);
		this.personalNr = personalNr;
	}

	public String getPersonalNr() {
		return personalNr;
	}

	public void setPersonalNr(String personalNr) {
		this.personalNr = personalNr;
	}
	
	public void aktualisiereInventar() {
		
	}

	@Override
	public String toString() {
		return "Administrator [name=" + name + ", email=" + email + ", benutzerId=" + benutzerId + ", passwort=" + passwort + "personalNr=" + personalNr + "]";
	}
}
