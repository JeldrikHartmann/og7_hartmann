package main;

public class Bankverbindung {

	private String kontoNr;
	private String pin;
	public Bankverbindung() {
		super();
	}
	public Bankverbindung(String kontoNr, String pin) {
		super();
		this.kontoNr = kontoNr;
		this.pin = pin;
	}
	public String getKontoNr() {
		return kontoNr;
	}
	public void setKontoNr(String kontoNr) {
		this.kontoNr = kontoNr;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	@Override
	public String toString() {
		return "Bankverbindung [kontoNr=" + kontoNr + ", pin=" + pin + "]";
	}
}
