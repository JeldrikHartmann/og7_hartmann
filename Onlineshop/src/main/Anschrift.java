package main;

public class Anschrift {

	private String adresse;
	private String stadt;
	private String plz;
	public Anschrift() {
		super();
	}
	public Anschrift(String adresse, String stadt, String plz) {
		super();
		this.adresse = adresse;
		this.stadt = stadt;
		this.plz = plz;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getStadt() {
		return stadt;
	}
	public void setStadt(String stadt) {
		this.stadt = stadt;
	}
	public String getPlz() {
		return plz;
	}
	public void setPlz(String plz) {
		this.plz = plz;
	}
	@Override
	public String toString() {
		return "Anschrift [adresse=" + adresse + ", stadt=" + stadt + ", plz=" + plz + "]";
	}
}
