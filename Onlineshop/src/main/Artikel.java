package main;

public class Artikel {

	private String artikelId;
	private String name;
	private int preis;
	public Artikel() {
		super();
	}
	public Artikel(String artikelId, String name, int preis) {
		super();
		this.artikelId = artikelId;
		this.name = name;
		this.preis = preis;
	}
	public String getArtikelId() {
		return artikelId;
	}
	public void setArtikelId(String artikelId) {
		this.artikelId = artikelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPreis() {
		return preis;
	}
	public void setPreis(int preis) {
		this.preis = preis;
	}
	@Override
	public String toString() {
		return "Artikel [artikelId=" + artikelId + ", name=" + name + ", preis=" + preis + "]";
	}
}
