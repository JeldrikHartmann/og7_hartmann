package main;

public abstract class Benutzer {

	protected String name;
	protected String email;
	protected String benutzerId;
	protected String passwort;

	public Benutzer() {
	}

	public Benutzer(String name, String email, String benutzerId, String passwort) {
		this.name = name;
		this.email = email;
		this.benutzerId = benutzerId;
		this.passwort = passwort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBenutzerId() {
		return benutzerId;
	}

	public void setBenutzerId(String benutzerId) {
		this.benutzerId = benutzerId;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public void einloggen(){
		
	}

	@Override
	public String toString() {
		return "Benutzer [name=" + name + ", email=" + email + ", benutzerId=" + benutzerId + ", passwort=" + passwort
				+ "]";
	}
}
