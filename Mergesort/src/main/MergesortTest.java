package main;

public class MergesortTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Mergesort b1 = new Mergesort();
		int[] zahlenliste = { 5, 10, 3, 8, 0 , 4, 6, 2, 1, 7, 9 };
		System.out.println("MergeSort:\n");
		System.out.println("-----------\n");
		System.out.print("Start  ");
		System.out.println(b1.gibArrayAus(zahlenliste));
		b1.mergesort(zahlenliste, 0, zahlenliste.length - 1);
		System.out.print("Ende  ");
		System.out.println(b1.gibArrayAus(zahlenliste));
	}
}
