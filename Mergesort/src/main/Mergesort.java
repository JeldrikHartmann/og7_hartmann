package main;

public class Mergesort {
	public void mergesort(int[] a, int l, int r) {
		if (l < r) {
			int q = (l + r) / 2;
			mergesort(a, l, q);
			mergesort(a, q + 1, r);
			merge(a, l, q, r);

		} else {

		}
	}

	void merge(int a[], int l, int q, int r) {
		int[][] b = new int[2][a.length];
		for (int i = l; i <= q; i++) {

			b[0][i] = i;
			b[1][i] = i;
		}
		for (int j = q + 1; j <= r; j++) {
			b[0][j] = r + q + 1 - j;
			b[1][j] = r + q + 1 - j;
		}
		int i = l;
		int j = r;
		for (int k = l; k <= r - 1; k++) {
			int s = b[0][i];
			int t = b[0][j];
			if (a[s] <= a[t]) {
				i++;
			} else {
				s = t;
				j--;
			}
			int n = a[k];
			a[k] = a[s];
			a[s] = n;
			t = b[1][k];
			b[0][t] = s;
			b[1][s] = t;
			System.out.print(k + " <-> " + s + "  ");
			System.out.println(gibArrayAus(a));
		}
	}

	public String gibArrayAus(int[] a) {
		System.out.print("{ ");
		for (int i = 0; i < a.length - 1; i++)
			System.out.print(a[i] + ", ");
		return a[a.length - 1] + " }\n";
	}
}
