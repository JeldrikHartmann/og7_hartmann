package OSZ_Kickers;

public class Person {

	protected String name;
	protected String telefonnummer;
	protected boolean isBeitragGezahlt;
	
	public Person() {
	}

	public Person(String name, String telefonnummer, boolean isBeitragGezahlt) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.isBeitragGezahlt = isBeitragGezahlt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isBeitragGezahlt() {
		return isBeitragGezahlt;
	}

	public void setBeitragGezahlt(boolean isBeitragGezahlt) {
		this.isBeitragGezahlt = isBeitragGezahlt;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", telefonnummer=" + telefonnummer + ", isBeitragGezahlt=" + isBeitragGezahlt
				+ "]";
	}
	
	
}
