package OSZ_Kickers;

public class Mannschaftsleiter extends Spieler{
	
	private String mannschaftsname;
	private boolean engagement;

	public Mannschaftsleiter() {
		
	}

	public Mannschaftsleiter(String name, String telefonnummer, boolean isBeitragGezahlt, int trikotnummer, String spielposition, String mannschaftsname, boolean engagement) {
		super(name, telefonnummer, isBeitragGezahlt, trikotnummer, spielposition);
		this.mannschaftsname = mannschaftsname;
		this.engagement = engagement;
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public boolean isEngagement() {
		return engagement;
	}

	public void setEngagement(boolean engagement) {
		this.engagement = engagement;
	}

	@Override
	public String toString() {
		return "Mannschaftsleiter [name=" + name + ", telefonnummer=" + telefonnummer + ", isBeitragGezahlt=" + isBeitragGezahlt + ", trikotnummer=" + trikotnummer + ", spielposition=" + spielposition + ", mannschaftsname=" + mannschaftsname + ", engagement=" + engagement + "]";
	}

}
