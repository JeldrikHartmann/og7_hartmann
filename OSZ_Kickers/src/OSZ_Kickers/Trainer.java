package OSZ_Kickers;

public class Trainer extends Person{
	
	private char lizenzklasse;
	private int aufwandsentsch�digung;

	public Trainer() {
		// TODO Auto-generated constructor stub
	}

	public Trainer(String name, String telefonnummer, boolean isBeitragGezahlt, char lizenzklasse, int aufwandsentsch�digung) {
		super(name, telefonnummer, isBeitragGezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwandsentsch�digung = aufwandsentsch�digung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandsentsch�digung() {
		return aufwandsentsch�digung;
	}

	public void setAufwandsentsch�digung(int aufwandsentsch�digung) {
		this.aufwandsentsch�digung = aufwandsentsch�digung;
	}

	@Override
	public String toString() {
		return "Trainer [name=" + name + ", telefonnummer=" + telefonnummer + ", isBeitragGezahlt=" + isBeitragGezahlt + ", lizenzklasse=" + lizenzklasse + ", aufwandsentsch�digung=" + aufwandsentsch�digung + "]";
	}

}
