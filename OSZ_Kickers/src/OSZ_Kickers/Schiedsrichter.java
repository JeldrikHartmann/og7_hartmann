package OSZ_Kickers;

public class Schiedsrichter extends Person{

	private int anzSpiele;
	
	public Schiedsrichter() {
	}

	public Schiedsrichter(String name, String telefonnummer, boolean isBeitragGezahlt, int anzSpiele) {
		super(name, telefonnummer, isBeitragGezahlt);
		this.anzSpiele = anzSpiele;
	}

	public int getAnzSpiele() {
		return anzSpiele;
	}

	public void setAnzSpiele(int anzSpiele) {
		this.anzSpiele = anzSpiele;
	}

	@Override
	public String toString() {
		return "Schiedsrichter [name=" + name + ", telefonnummer=" + telefonnummer + ", isBeitragGezahlt=" + isBeitragGezahlt + ", anzSpiele=" + anzSpiele + "]";
	}

	
}
