package OSZ_Kickers;

public class TestKickers {


	public static void main(String[] args) {
		
		Spieler s1 = new Spieler("Jeldrik", "0305437950", true, 14, "MITTE");
		Schiedsrichter sch1 = new Schiedsrichter("Henrik", "03058290488", false, 20);
		Trainer t1 = new Trainer("Dennis", "03053490650", false, 'A', 450);
		Mannschaftsleiter m1 = new Mannschaftsleiter("Pascal", "030574280", true, 88, "TOR", "OSZ TORnado Kickers 1996", true);

		System.out.println(s1.toString());
		System.out.println(sch1.toString());
		System.out.println(t1.toString());
		System.out.println(m1.toString());
	}

}
