package OSZ_Kickers;

public class Spieler extends Person{
	
	protected int trikotnummer;
	protected String spielposition;

	public Spieler() {
	}

	public Spieler(String name, String telefonnummer, boolean isBeitragGezahlt, int trikotnummer, String spielposition) {
		super(name, telefonnummer, isBeitragGezahlt);
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}


	public String toString() {
		return "Spieler [name=" + name + ", telefonnummer=" + telefonnummer + ", isBeitragGezahlt=" + isBeitragGezahlt + ", trikotnummer=" + trikotnummer + ", spielposition=" + spielposition + "]";
	}

}
