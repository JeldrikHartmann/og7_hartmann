package main;

import java.util.ArrayList;
import java.util.Scanner;

public class KeyStoreTest {

	static void menue() {
		System.out.println("\n ***** Key-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) zeigen");
		System.out.println(" 5) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String args[]) {
		@SuppressWarnings("resource")
		Scanner myScanner = new Scanner(System.in);
		KeyStore02 k1 = new KeyStore02();
		char wahl;
		String eintrag;
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.print("\nSchl�ssel: ");
				eintrag = myScanner.next();
				if (k1.add(eintrag))			
					System.out.println("Schl�ssel wurde eingetragen\n");
				else
					System.out.println("KeyStore voll \n");
				break;
			case '2':
				System.out.print("\nGesuchter Schl�ssel: ");
				eintrag = myScanner.next();
				index = k1.indexOf(eintrag);
				if (index >= 0)
					System.out.println("Eintrag wurde an " + index + ". Stelle gefunden\n");
				else
					System.out.println("Eintrag nicht gefunden\n");
				break;
			case '3':
				System.out.println("Aktuelle L�nge der Liste: " + k1.size());
				System.out.println("Liste: \n  [" + k1.toString() + "]");
				System.out.print("\nZu l�schender Eintrag: ");
				eintrag = myScanner.next();
				index = k1.indexOf(eintrag);
				if (index >= 0) {
					k1.remove(index);
					System.out.println("Eintrag wurde gel�scht\n");
				} else
					System.out.println("Eintrag nicht gefunden\n");
				break;
			case '4':
				System.out.println("Aktuelle L�nge der Liste: " + k1.size());
				System.out.println("Liste: \n  [" + k1.toString() + "]");
				break;
			case '5':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 5);
	}

	private static void KeyStore02() {
		// TODO Auto-generated method stub
		
	}// main
}// ZahlenspeicherTest
