package main;

import java.util.Random;

public class Interpolationssuche {

	private final int NICHT_GEFUNDEN = -1;

	public Interpolationssuche() {
	}

	public long[] getSortedList(long[] zahlenliste) {
		Random rand = new Random(111111);

		long naechsteZahl = 0;

		for (int i = 0; i < zahlenliste.length; i++) {
			naechsteZahl += rand.nextInt(3) + 1;
			zahlenliste[i] = naechsteZahl;
		}

		return zahlenliste;
	}

	public long suche(long[] zahlenliste, int von, int bis, long gesuchteZahl) {
		if (von < bis) {
			int bereich = (int) (zahlenliste[bis] - zahlenliste[von]);
			int pivot = (int) (von + (bis - von) * (1.0 * (gesuchteZahl - zahlenliste[von]) / bereich));


			if (zahlenliste.length == 0) {
				return NICHT_GEFUNDEN;
			}

			if (pivot >= zahlenliste.length) {
				return NICHT_GEFUNDEN;
			}
			
			if (gesuchteZahl > zahlenliste[pivot]) {
				return suche(zahlenliste, pivot + 1, bis, gesuchteZahl);
			} else if (gesuchteZahl < zahlenliste[pivot]) {
				return suche(zahlenliste, von, pivot - 1, gesuchteZahl);
			} else if (gesuchteZahl == zahlenliste[pivot]) {
				return pivot;
			} else {
				return NICHT_GEFUNDEN;
			}			
		}
		return NICHT_GEFUNDEN;
	}

	public int lineareSuche(long[] array, long gesuchteZahl) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == gesuchteZahl)
				return i;
		return -1;
	}
}