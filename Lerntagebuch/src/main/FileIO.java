package main;

import java.io.*;

public class FileIO {

	public void dateiEinlesenUndAusgeben(File file) {
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String s = br.readLine();
			while(s != null) {
				System.out.println(s);
				s = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {

		String path = System.getProperty("user.dir");
		File file = new File(path + "/src/main/miriam.dat");
		FileIO dl = new FileIO();
		dl.dateiEinlesenUndAusgeben(file);
		
	}
}
