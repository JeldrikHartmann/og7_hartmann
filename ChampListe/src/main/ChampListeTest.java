package main;

import java.util.ArrayList;
import java.util.Arrays;

public class ChampListeTest {

	public static void main(String[] args) {
		ArrayList<String> cl = new ArrayList<>();
		cl.addAll(Arrays.asList("Zac", "Amumu", "Anivia", "Annie", "Ashe", "Aurelion Sol", "Azir", "Bard", "Blitzcrank",
				"Brand", "Braum", "Caitlyn", "Camille", "Cassiopeia", "Cho'Gath", "Corki", "Darius", "Tryndamere",
				"Twisted Fate", "Diana", "Dr. Mundo", "Draven", "Ekko", "Elise", "Evelynn", "Ezreal", "Fiddlesticks",
				"Fiora", "Fizz", "Galio", "Gangplank", "Garen", "Gnar", "Gragas", "Graves", "Hecarim", "Ivern", "Janna",
				"Jarvan IV.", "Jax", "Jayce", "Jhin", "Jinx", "Kai'Sa", "Kalista", "Karma", "Karthus", "Kassadin",
				"Katarina", "Kayle", "Kayn", "Kennen", "Kha'Zix", "Kindred", "Kled", "Kog'Maw", "LeBlanc", "Lee Sin",
				"Leona", "Lissandra", "Lucian", "Lulu", "Lux", "Malphite", "Malzahar", "Maokai", "Master Yi",
				"Miss Fortune", "Mordekaiser", "Morgana", "Nami", "Nasus", "Nautilus", "Neeko", "Nidalee", "Nocturne",
				"Nunu & Willump", "Heimerdinger", "Illaoi", "Irelia", "Olaf", "Orianna", "Ornn", "Pantheon", "Poppy",
				"Zed", "Ziggs", "Zilean", "Zoe", "Zyra", "Pyke", "Quinn", "Rakan", "Rammus", "Rek'Sai", "Renekton",
				"Rengar", "Riven", "Rumble", "Ryze", "Sejuani", "Shaco", "Shen", "Shyvana", "Singed", "Sion", "Sivir",
				"Skarner", "Sona", "Soraka", "Swain", "Syndra", "Tahm Kench", "Taliyah", "Talon", "Taric", "Teemo",
				"Thresh", "Tristana", "Trundle", "Twitch", "Udyr", "Urgot", "Varus", "Vayne", "Veigar", "Vel'Koz", "Vi",
				"Viktor", "Vladimir", "Volibear", "Warwick", "Wukong", "Xayah", "Xerath", "Xin Zhao", "Yasuo", "Yorick",
				"Aatrox", "Ahri", "Akali", "Alistar"));

		System.out.println(cl.toString());
	}

}
