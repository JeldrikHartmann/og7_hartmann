package main;

import java.util.Arrays;

/**
 *
 * �bungsklasse zu Feldern
 *
 * @version 1.0 vom 05.05.2011
 * @author Tenbusch
 */

public class Felder {

	// unsere Zahlenliste zum Ausprobieren
	private int[] zahlenliste = { 5, 8, 4, 3, 9, 1, 2, 7, 6, 0 };

	// Konstruktor
	public Felder() {
	}

	// Methode die Sie implementieren sollen
	// ersetzen Sie den Befehl return 0 durch return ihre_Variable

	public int[] getZahlenliste() {
		return zahlenliste;
	}

	public void setZahlenliste(int[] zahlenliste) {
		this.zahlenliste = zahlenliste;
	}

	// die Methode soll die gr��te Zahl der Liste zur�ckgeben
	public int maxElement() {

		int max = 0;
		for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i] > max) {
				max = zahlenliste[i];
			}
		}
		return max;
	}

	// die Methode soll die kleinste Zahl der Liste zur�ckgeben
	public int minElement() {

		int min = 0;
		for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i] < min) {
				min = zahlenliste[i];
			}
		}
		return min;
	}

	// die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
	public int durchschnitt() {
		int ds = 0;
		for (int i = 0; i < zahlenliste.length; i++) {
			ds = ds + zahlenliste[i];
		}
		ds = ds / zahlenliste.length;
		return ds;
	}

	// die Methode soll die Anzahl der Elemente zur�ckgeben
	// der Befehl zahlenliste.length; k�nnte hierbei hilfreich sein
	public int anzahlElemente() {
		int l�nge = zahlenliste.length;
		return l�nge;
	}

	// die Methode soll die Liste ausgeben
	public String toString() {
		String ausgabe = "";
		for (int i = 0; i < zahlenliste.length; i++) {
			ausgabe = ausgabe + zahlenliste[i];
		}
		return ausgabe;
	}

	// die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
	// dem Feld vorhanden ist
	public boolean istElement(int zahl) {
		boolean istZahl = false;
		for (int i = 0; i < zahlenliste.length; i++)
			if (zahl == i) {
				istZahl = true;
			}
		return istZahl;
	}

	// die Methode soll das erste Vorkommen der
	// als Parameter �bergebenen Zahl liefern oder -1 bei nicht vorhanden
	public int getErstePosition(int zahl) {
		int position = -1;
		for (int i = 0; i < zahlenliste.length; i++) {
			if(zahl == zahlenliste[i]) {
				position = i;
			}
		}
		return position;
	}

	// die Methode soll die Liste aufsteigend sortieren
	// googlen sie mal nach Array.sort() ;)
	public void sortiere() {
		Arrays.sort(zahlenliste);
	}

	public static void main(String[] args) {
		Felder testenMeinerL�sung = new Felder();
		System.out.println(testenMeinerL�sung.maxElement());
		System.out.println(testenMeinerL�sung.minElement());
		System.out.println(testenMeinerL�sung.durchschnitt());
		System.out.println(testenMeinerL�sung.anzahlElemente());
		System.out.println(testenMeinerL�sung.toString());
		System.out.println(testenMeinerL�sung.istElement(9));
		System.out.println(testenMeinerL�sung.getErstePosition(8));
		testenMeinerL�sung.sortiere();
		System.out.println(testenMeinerL�sung.toString());
	}
}
