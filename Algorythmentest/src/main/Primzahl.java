package main;

public class Primzahl {

	public boolean isPrimzahl(long zahl) {
		if (zahl != 2 && zahl % 2 != 0) {
			for (long i = 3; i <= zahl / 2; i = i + 2) {
				if (zahl % i == 0) {
					return false;
				}

			}
		} else if (zahl == 2) {
			return true;
		} else {
			return false;
		}
		return true;
	}
}
