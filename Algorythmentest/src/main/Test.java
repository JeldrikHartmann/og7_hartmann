package main;


public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stoppuhr s1 = new Stoppuhr();
		Primzahl p1 = new Primzahl();
		long[] primzahlenliste = { 2, 101, 15485867, 86028157, 179424691, 373587911, 472882049, 553105253, 715225741,
				982451653 };
		for (int i = 0; i < primzahlenliste.length; i++) {
			s1.start();
			System.out.println("Primzahl " + (i + 1) + ": " + p1.isPrimzahl(primzahlenliste[i]));
			s1.stop();
			System.out.println(s1.getMillis() + "ms");
			System.out.println("");
			s1.reset();

		}
	}
}