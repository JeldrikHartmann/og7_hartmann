package main;

public class Stoppuhr {

	private long start = 0;
	private long stop;

	public Stoppuhr(long stop) {
		this.stop = stop;
	}

	public Stoppuhr() {
	}

	public void start() {
		this.start = System.currentTimeMillis();
		}
	

	public void stop() {
		this.stop = System.currentTimeMillis();
		this.stop = this.stop - this.start;
	}

	public void reset() {
		this.start = 0;
		this.stop = 0;
	}

	public long getMillis() {
		return stop;

	}
}
